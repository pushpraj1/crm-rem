<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'api','namespace' => 'Api'], function () {
	Route::get('/auth/AllCountries', 'ApiController@AllCountries');
	Route::get('/auth/AllCurrency', 'ApiController@AllCurrency');
    Route::get('PageFile','ApiController@create');
    Route::post('/auth/register','ApiController@register');
    Route::post('/auth/login','ApiController@login');
   	Route::group(['middleware' => 'auth'], function() {
    Route::post('logout','UserapiController@destroy');
   }); 
});

