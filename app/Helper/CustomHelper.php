<?php

namespace App\Helper;
use App\Countries;
use DB;

class CustomHelper {

	public static function GetCountries()
	{
        $data = Countries::select('ISOCode2','CountryName')->get();
        return $data;
	}

	public static function GetCurrency()
	{
        $data = Countries::select('CurrencyCode')->groupBy('CurrencyCode')->get();
        return $data;
	}

}
?>