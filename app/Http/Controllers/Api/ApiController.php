<?php

namespace App\Http\Controllers\Api;
header('Access-Control-Allow-Origin: *');  
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Mail;
use App\User;
use App\Subdomain;
use DB;
use App\Helper\CustomHelper;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     private $user;

    public function __construct() {
        $this->middleware('guest');
    }
    
    public function register()
    {
      
        $note = Input::json();
        $msg ='';
        $headers ='';
        $name =$note->get('name');
        $email =$note->get('email');
        $password =$note->get('password');
        $subdomain = $note->get('subdomain');
        $regiter = $note->get('regiter');
        $user_type = $note->get('user_type');
        $address = $note->get('address');
        $street = $note->get('street');
        $city = $note->get('city');
        $country = $note->get('country');
        $postel_code = $note->get('postel_code');
        $currency = $note->get('currency');
        $web = $note->get('web');
        $timezone = $note->get('timezone');
        $office_no = $note->get('office_no');
        $phone = $note->get('phone');
        $job_position = $note->get('job_position');
        $no_employee = $note->get('no_employee');
        $logo =$note->get('logo');
        if($regiter == 1){
            $data =array(
                'name'=>$name,
                'email'=>$email,
                'password'=>$password,
                'job_position'=>$job_position,
                'office_no'=>$office_no,
                'logo'=>$logo,
                'phone'=>$phone,
                );
            $validation_sub ='';
            $validation_job_position = 'required';
            $validation_office_no = 'required';
            $validation_address = '';
            $validation_street = '';
            $validation_city = '';
            $validation_country = '';
            $validation_postel_code = '';
            $validation_currency = '';
            $validation_web = '';
            $validation_no_employee = '';
            $user_type =1;
        }elseif($regiter == 0){
            $data =array(
                'name'=>$name,
                'email'=>$email,
                'password'=>$password,
                'subdomain'=>$subdomain,
                'address'=>$address,
                'street'=>$street,
                'city'=>$city,
                'country'=>$country,
                'postel_code'=>$postel_code,
                'web'=>$web,
                // 'timezone'=>$timezone,
                'no_employee'=>$no_employee,
                'logo'=>$logo,
                'phone'=>$phone,
                );
            $validation_address = 'required';
            $validation_street = 'required';
            $validation_city = 'required';
            $validation_country = 'required';
            $validation_postel_code = 'required';
            $validation_currency = 'required';
            $validation_web = 'required';
            $validation_job_position = '';
            $validation_office_no = '';
            $validation_no_employee = 'required';
            $validation_sub ='required';
            $user_type =0;
        }else{
            return json_encode(array('status'=>0,'msg'=>'Wrong Regiter Method !'));
        }
        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required',
            'phone' => 'required',
            'logo'=>'required',
            'subdomain' => $validation_sub,
            'job_position' => $validation_job_position,
            'office_no' => $validation_office_no,
            'address' => $validation_address,
            'street' => $validation_street,
            'city' => $validation_city,
            'country' => $validation_country,
            'postel_code' => $validation_postel_code,
            'web' => $validation_web,
            'no_employee' => $validation_no_employee,
        ]);

        if($validator->fails()){
            return json_encode(array('status'=>0,'msg'=>$validator->errors()));
        }else{
            if($regiter == 0){
                $responce = DB::table('subdomain')->where('subdomain_name',$subdomain)->first();
                // var_dump($responce);exit;
                if($responce !=''){
                    return json_encode(array('status'=>0,'msg'=>'Subdomain is Not Available!'));
                }
            }

            $user = DB::table('users')->insertGetId([
                    'name' => trim($name),
                    'email' => trim($email),
                    'subdomain' => trim($subdomain),
                    'password' => bcrypt(trim($password)),
                    'photo_url' => trim($logo),
                    'phone' => trim($phone),
                    'address' => trim($address),
                    'street' => trim($street),
                    'city' => trim($city),
                    'zip_code' => trim($postel_code),
                    'country_code' => trim($country),
                    'currency' => trim($currency),
                    'web' => trim($web),
                    'no_employee' => trim($no_employee),
                    'job_position'=>trim($job_position),
                    'office_no'=>trim($office_no),
                    'timezone' => $timezone,
                    'user_type' => $user_type,
                    'Verified' => 1,
                    'status' => 1,
                    'created_at'=>date('Y-m-d H:i:s'),
            ]);

            $userID = $user;
            if($regiter != 1){
              $newtableschema = array(
                 'tablename' => $subdomain.'_users',
                 'colnames' => array('user_id','name', 'email','subdomain','password','status','is_verify','user_type','photo_url','phone','address','street','city','zip_code','country_code','currency','web','no_employee','job_position','office_no','timezone'),
              );

            Schema::create($newtableschema['tablename'], function($table) use($newtableschema) {
                $table->increments('id')->unique(); //primary key        
                foreach($newtableschema['colnames'] as $col){
                  if($col == 'password' && $col == 'photo_url' && $col == 'address' && $col == 'web'){
                    $table->text($col);
                  }elseif($col == 'user_id' || $col == 'is_verify'|| $col == 'status'|| $col == 'user_type'){
                    $table->integer($col);
                  }else{
                    $table->string($col);
                  }
                }
                $table->timestamps();
            });
            $subuser = DB::table($subdomain.'_users')->insertGetId([
                    'name' => trim($name),
                    'user_id' => trim($userID),
                    'email' => trim($email),
                    'subdomain' => trim($subdomain),
                    'password' => bcrypt(trim($password)),
                    'photo_url' => trim($logo),
                    'phone' => trim($phone),
                    'address' => trim($address),
                    'street' => trim($street),
                    'city' => trim($city),
                    'zip_code' => trim($postel_code),
                    'country_code' => trim($country),
                    'currency' => trim($currency),
                    'web' => trim($web),
                    'no_employee' => trim($no_employee),
                    'job_position'=>trim($job_position),
                    'office_no'=>trim($office_no),
                    'timezone' => $timezone,
                    'status' => 1,
                    'is_verify' =>1,
                    'user_type' =>$user_type,
                    'created_at'=>date('Y-m-d H:i:s'),
                ]);
                $subdomain = DB::table('subdomain')->insert([
                    'user_id' => trim($userID),
                    'subdomain_name' => trim($subdomain),
                    'created_at'=>date('Y-m-d H:i:s'),
                ]);
            }

            // $userID = $user;
            // $to = $email;
            // $to = 'singh.puspraj2713@gmail.com';
            // $subject = "Welcome to Remarso-CRM";
            // $msg .= "Hello ".$email;
            // $msg .= "<br>";
            // $msg .= "<br>";
            // $msg .= "URL :- ".$subdomain.".rem-crm.com";
            // $msg .= "<br>";
            // $msg .= "URL :-".$subdomain.".rem-crm.com";
            // $msg .= "<br>";
            // $msg .= "Username :- ".$email;
            // $msg .= "<br>";
            // $msg .= "<br>";
            // $msg .= "<br>";
            // $msg .= "Thank you";
            // $msg .= "<br>";
            // $msg .= "<br>";
            // $msg .= "With Regards";
            // $msg .= "<br>";
            // $msg .= "Remso-Crm0";
            // $headers .= "MIME-Version: 1.0" . "\r\n";
            // $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            // $headers .= "From:notifications@rem-crm.com" . "\r\n";
            // $responce=mail($to,$subject,$msg,$headers);
              // $data = array('name'=>"Pushpraj Singh Chouhan");
              // Mail::send('mail', $data, function($message) {
              //    $message->to('singh.puspraj2713@gmail.com', 'check Mail')->subject
              //       ('Laravel HTML Testing Mail');
              //    $message->from('pushpraj@mobiwebtech.com','Pushpraj Singh Chouhan');
              // });
            if (!$userID) {
                return json_encode(array('status'=>0,'msg'=>'Internal server erorr,please try again later !'));
            }else{
                return json_encode(array('status'=>1,'msg'=>'User register Successfully !','user_id'=>$userID));
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $note = Input::json();
            $logindetails = array('email'=>$note->get('email'),'password'=>$note->get('password'));
             try {
               if(!$token = JWTAuth::attempt($logindetails)){
                    return json_encode(array('status'=>0,'msg'=>'Invalid login details, please try again.'));
                }
             }catch(JWTException $ex){
                return json_encode(array('status'=>0,'msg'=>'Some error occurred, please try again.'));
             }
            $userdata = User::where('email', '=', $note->get('email'))->where('UserType', '=', $note->get('user_type'))->first();
            $response = array();
            if($userdata !=''){
                if($userdata->Verified == 0){
                    return json_encode(array('status'=>0,'msg'=>'Email ID not verified, please verified first.'));
                }else{
                     if (Auth::attempt(['email' =>$note->get('email'), 'password' =>$note->get('password')])) {
                       
                     }
            }
        }else{
            return json_encode(array('status'=>0,'msg'=>'Invalid login details, please try again.'));
        }
    }

    function AllCountries(){
        $country= CustomHelper::GetCountries();
        if(!empty($country)){
            return json_encode(array('status'=>1,'list'=>$country));
        }else{
            return json_encode(array('status'=>0,'list'=>$country));
        }
    }

    function AllCurrency(){
        $country= CustomHelper::GetCurrency();
        if(!empty($country)){
            return json_encode(array('status'=>1,'list'=>$country));
        }else{
            return json_encode(array('status'=>0,'list'=>$country));
        }
    }
   
}
