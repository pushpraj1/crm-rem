<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
 	protected $fillable = [
        'CountryName',
        'ISOCode2',
        'ISOCode3',
        'ISONumeric',
        'CountryPhoneCode',
        'CurrencyCode',
        'CurrencyName',
        'CurrencySymbol',
        'CountryFlag',
    ];
}
